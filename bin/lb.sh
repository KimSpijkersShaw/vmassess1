#!/bin/bash
yum -y install haproxy nano
cp -f /vagrant/haproxy.cfg /etc/haproxy
systemctl enable haproxy
systemctl start haproxy

if rpm -q haproxy
then
  echo "haproxy intalled successfully"
else
  echo "haproxy install failed" 1>&2
  exit 1
fi

web1=$(curl localhost) >/dev/null
web2=$(curl localhost) >/dev/null

if [[ $web1 = *"My IP address is 192.168.51.2"* ]] && [[ $"My IP address is 192.168.51.20"* ]]
then
  echo "Load balancer working"
elif [[ $web2 = *"My IP address is 192.168.51.20"* ]] && [[ $web1 = *"My IP address is 192.168.51.2"* ]]
then
  echo "Load balancer working"
else
  echo "Load balancer failed to find correct webpages" 1>&2
  exit 3
fi