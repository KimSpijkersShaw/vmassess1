#!/bin/bash
apt -y update
apt -y install apache2 nano

/vagrant/web_start_ubuntu

#Check apache has installed
if dpkg -s apache2 | grep "install ok installed" >/dev/null
then
  echo "apache intalled successfully"
else
  echo "apache install failed" 1>&2
  exit 1
fi

#Check that correct webpage is being shown
if curl localhost | grep "My IP address is 192.168.51.2" >/dev/null
then
  echo "Webpage loading correctly"
else
  echo "Webpage not loading correctly" 1>&2
  exit 2
fi