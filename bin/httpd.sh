#!/bin/bash
yum -y install httpd
systemctl start httpd
systemctl enable httpd

/vagrant/web_start_centos

#Check apache has installed
if rpm -q httpd >/dev/null
then
  echo "httpd intalled successfully"
else
  echo "httpd install failed" 1>&2
  exit 1
fi

#Check that correct webpage is being shown
if curl localhost | grep "My IP address is 192.168.51.20" >/dev/null
then
  echo "Webpage loading correctly"
else
  echo "Webpage not loading correctly" 1>&2
  exit 2
fi

#Check that can connect to other webserver over private network
if ping -c 2 192.168.51.2 >/dev/null
then
  echo "connected to webserver at 192.168.51.2"
else
  echo "cannot connect see webserver at 192.168.51.2" 1>&2
  exit 4
fi

